<?php

namespace App\Http\Controllers;

use App\Models\Note;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class NoteController extends Controller
{

    /**
     * @return Note[]|\Illuminate\Database\Eloquent\Collection
     */
    public function get() {
        return Note::all(['id', 'name']);
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request): Response
    {
        $this->validate($request, self::nameMaxOneHundredSymbolsLength());
        $note = new Note();
        $note->fill($request->all());
        $note->save();
        return response()->make('Created', 201);
    }

    /**
     * @param string $noteId
     * @param Request $request
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(string $noteId, Request $request): Response
    {
        $this->validate($request, self::nameMaxOneHundredSymbolsLength());
        try{
            $note = Note::find($noteId);
            $note->fill($request->all());
            $note->save();
            return response()->make('OK');
        } catch (\Exception $e) {
            return response()->make($e->getMessage(), 500);
        }
    }

    /**
     * @param string $noteId
     * @return Response
     */
    public function delete(string $noteId): Response
    {
        try{
            $note = Note::findOrFail($noteId);
            try {
                $note->delete();
                return response()->make('No Content', 204);
            } catch (\Exception $e) {
                return response()->make($e->getMessage(), 500);
            }
        } catch (ModelNotFoundException $e) {
            return response()->make('not found note', 404);
        }
    }

    /**
     * @return Response
     */
    public function deleteAll(): Response
    {
        try {
            Note::all()->each(function (Note $note) {
                $note->delete();
            });
            return response()->make('No Content', 204);
        } catch (\Exception $e) {
            return response()->make($e->getMessage(), 500);
        }
    }

    private static function nameMaxOneHundredSymbolsLength(): array
    {
        return ['name' => ['required', 'max:100']];
    }


}
