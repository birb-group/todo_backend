<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskController extends Controller
{

    /**
     * @return Task[]|\Illuminate\Database\Eloquent\Collection
     */
    public function get() {
        return Task::all(['id', 'subject', 'note_id', 'resolved']);
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request): Response
    {
        $this->validate($request, self::taskValidationRules() + ['subject' => ['required']]);
        $task = new Task();
        $task->fill($request->all());
        $task->forceFill(['note_id' => $request->get('note_id')]);
        $task->save();
        return response()->make('Created', 201);
    }

    /**
     * @param Request $request
     * @param int $taskId
     * @return Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, int $taskId): Response
    {
        $rules = self::taskValidationRules();
        $rules['note_id'][] = ['nullable'];
        $this->validate($request, $rules + ['subject' => ['nullable']]);
        try{
            $task = Task::find($taskId);
            $task->fill($request->all());
            if($request->has('note_id')) {
                $task->forceFill(['note_id' => $request->get('note_id')]);
            }
            $task->save();
            return response()->make('OK');
        } catch (\Exception $e) {
            return response()->make($e->getMessage(), 500);
        }
    }

    /**
     * @param string $taskId
     * @return Response
     */
    public function delete(string $taskId): Response
    {
        try {
            Task::find($taskId)->delete();
            return response()->make('No Content', 204);
        } catch (\Exception $e) {
            return response()->make($e->getMessage(), 500);
        }
    }

    /**
     * @return Response
     */
    public function deleteAll(): Response
    {
        try {
            Task::truncate();
            return response()->make('No Content', 204);
        } catch (\Exception $e) {
            return response()->make($e->getMessage(), 500);
        }

    }

    private static function taskValidationRules(): array
    {
        return ['note_id' => ['exists:notes,id'], 'resolved' => ['nullable', 'boolean']];
    }

}
