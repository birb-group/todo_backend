<?php

namespace App\Utils;

use Laravel\Lumen\Routing\Router;

class RouteUtils
{

    public static function restFullGenerator(Router $router, string $routeName, string $routeCurrentName, string $controllerName): \Closure
    {
        return function () use ($routeCurrentName, $routeName, $controllerName, $router) {
            $router->get($routeName, [
                'as' => 'get', 'uses' => $controllerName . '@get'
            ]);
            $router->post($routeName, [
                'as' => 'create', 'uses' => $controllerName . '@create'
            ]);
            $router->put($routeCurrentName, [
                'as' => 'update', 'uses' => $controllerName . '@update'
            ]);
            $router->delete($routeCurrentName, [
                'as' => 'delete', 'uses' => $controllerName . '@delete'
            ]);
//            $router->delete($routeName, [
//                'as' => 'deleteAll', 'uses' => $controllerName . '@deleteAll'
//            ]);
        };
    }
}
