<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property boolean resolved
 * @property int note_id
 */
class Task extends Model
{

    protected $fillable = ['resolved', 'subject'];
    protected $casts = ['resolved' => 'boolean'];
    public $timestamps = false;

    /**
     * query builder owned note
     * @return BelongsTo
     */
    public function note(): BelongsTo
    {
        return $this->belongsTo(Note::class, 'note_id', 'id');
    }
}
