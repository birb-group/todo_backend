<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property boolean resolved
 */
class Note extends Model
{

    protected $fillable = ['name'];
    public $timestamps = false;
    protected $with = ['tasks'];

    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class, 'note_id', 'id');
    }

}
