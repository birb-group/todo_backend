<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Utils\RouteUtils;

$router->get('/', function () use ($router) {
    return 'API for todo test quest. Contact for start quest with Birb (telegram - https://t.me/dimabirddd, discord - Птица#9580)';
});

$router->group([], RouteUtils::restFullGenerator($router, '/note', '/note/{noteId}', 'NoteController'));
$router->group([], RouteUtils::restFullGenerator($router, '/task', '/task/{taskId}', 'TaskController'));
