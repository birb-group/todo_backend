<?php


class TaskTest extends TestCase
{
    /**
     * @return void
     */
    public function testGetTasks()
    {
        $this->get('/');

        $this->assertEquals(
            $this->app->version(), $this->response->getContent()
        );
    }
}
